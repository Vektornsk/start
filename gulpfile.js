/****
-- gulp-2017
****/
'use strict';

const gulp = require('gulp'),
      path = require('path'),
	  concat = require('gulp-concat'), //no
	  uglify = require('gulp-uglifyjs'), //no
	  cssnano = require('gulp-cssnano'), //no
	  rename  = require('gulp-rename'), //no
	  del = require('del'),
	  imagemin = require('gulp-imagemin'),
	  pngquant = require('imagemin-pngquant'), 
	  cache = require('gulp-cache'),
      sourcemaps = require('gulp-sourcemaps'),
      notify = require( 'gulp-notify' ),
      autoprefixer = require('gulp-autoprefixer'),
      browserSync = require('browser-sync').create(),
	  less = require('gulp-less'),
	  watcher = require('gulp-watch'),
      pug = require('gulp-pug');

const src = path.join(__dirname, 'src'),
	  libs = path.join(__dirname, 'node_modules');


gulp.task('default', ['browser-sync', 'watch']);


gulp.task('build', ['clean', 'pug', 'less', 'img', 'libs'], function() {

    let buildCss = gulp.src(src + '/css/style.css')
    .pipe(gulp.dest('dist/css'));

    let buildFonts = gulp.src(src + '/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'));

    let buildJs = gulp.src(src + '/js/**/*')
    .pipe(gulp.dest('dist/js'));

    let buildHtml = gulp.src(src + '/html/*.html') 
    .pipe(gulp.dest('dist/html'));

	let buildIndex = gulp.src(src + '/*.html') 
    .pipe(gulp.dest('dist'));

});

gulp.task('less', function(){
	function runLess() {
		return gulp.src(src + '/less/style.less')
		.pipe(sourcemaps.init())
		.pipe(less())
		.on('error', notify.onError({
			title: 'LESS ERROR',
			message: '@ <%= error.message%>'
		}))
		.pipe(autoprefixer({browsers: ['last 5 versions']}))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(src + '/css'))
		.pipe(browserSync.reload({stream: true}));
	}
	watcher(src + '/less/**/*', runLess);
    return runLess();
});

gulp.task('pug', function(){
	function runPug() {
		return gulp.src(src + '/pug/*.pug')
		.pipe(pug({
			pretty: true
		}))
		.on('error', notify.onError({
			title: 'JADE ERROR',
			message: '@ <%= error.message%>'
		}))
		.pipe(gulp.dest(src + '/html'));
	}
	watcher(src + '/pug/**/*', runPug);
    return runPug();
});

gulp.task('watch', function(){
    gulp.watch(src + '/html/*').on('change', browserSync.reload);
	gulp.watch(src + '/js/*').on('change', browserSync.reload);
});

gulp.task('browser-sync', ['pug', 'less', 'libs'],  function() {
	browserSync.init({
		server: {
			baseDir: src
		},
		notify: false
	});
});

gulp.task('img', function() {
	return gulp.src(src + '/img/**/*')
	.pipe(cache(imagemin({ 
		interlaced: true,
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
	})))
	.pipe(gulp.dest('dist/img'));
});

gulp.task('clear', function () {
    return cache.clearAll();
});

gulp.task('clean', function() {
    return del.sync('dist'); 
});

gulp.task('libs', function(){

	let jquery = gulp.src([
		libs + '/jquery/dist/jquery.min.js'
		])
	.pipe(gulp.dest(src + '/js/libs'));
})